import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Student } from "./StudentModel";

@Entity()
export class Division {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length:100
    })
    Divisionname: string;

    @Column()
    roomnumber: string;

    @OneToMany(() => Student, student => student.division)
    students: Student[];


}