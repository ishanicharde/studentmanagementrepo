import {Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn} from "typeorm";
import { Division } from "./DivisionModel";

@Entity()
export class Subject {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length:100
    })
    subjectname: string;

    @OneToOne(type => Division)
    @JoinColumn()
    division: Division;

    // @OneToMany(type => Division, division => division.subject) // note: we will create author property in the Photo class below
    // divisions: Division[];

}