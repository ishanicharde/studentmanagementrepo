import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class City {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length:100
    })
    cityname: string;

    @Column()
    pin: string;
    
    @Column()
    country: string;

}