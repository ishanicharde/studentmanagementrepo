import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, ManyToOne} from "typeorm";
import { City } from "./CityModel";
import { Division } from "./DivisionModel";

@Entity()
export class Student {
  
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        length:100
    })
    name: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column("text")
    phone: string;

    @Column()
    photo: string;

    @Column("text")
    address: string;

    @OneToOne(type => City)
    @JoinColumn()
    city: City;

    @OneToOne(type => Division)
    @JoinColumn()
    division: Division;

    // @ManyToOne(() => Division, division => division.students)
    // division: Division;

}