import {NextFunction, Request, Response, Router} from "express"; 
import {StudentRepository} from "../repository/student.repository";
import * as multer from "multer";

const upload = multer({dest:'file/'});

export class StudentController {

    // public route = "/file";
    // public router: Router = Router();
    // constructor(){
    //     this.router.post("/", this.upload);
    // }

    // private upload(req: Request, res: Response, next : NextFunction): void{
    //     const upload=multer({dest:"./upload"}).single("photo");
    //     console.log("photo="+upload);
    //     upload(req, res,(error)=>{
    //         if (error) {
    //             return error;
    //         }
    //        return "upload sucessfully"
    //     })
    //     return

    // }


    // const storage = multer.diskStorage({
    //     destination: function (req, file, cb) {
    //         cb(null, './uploads/')
    //     },
        
    //     filename: function (req: any, file: any, cb: any) {
    //         cb(null, file.originalname)
    //     }
    // });

    // const fileFilter = (req: any,file: any,cb: any) => {
    //     if(file.mimetype === "image/jpg"  || 
    //        file.mimetype ==="image/jpeg"  || 
    //        file.mimetype ===  "image/png"){
         
    //     cb(null, true);
    //    }else{
    //       cb(new Error("Image uploaded is not of type jpg/jpeg or png"),false);
    // }
    // }
    // const upload = multer({storage: storage, fileFilter : fileFilter});



 
    async all(req: Request, res: Response) {
        var stud= new StudentRepository();
        return  stud.findAll(req);
    } 
    
    async studentProfile(req: Request, res: Response) {
        var stud= new StudentRepository();
        return  stud.findOne(req,res);
    } 

    async update(req: Request, res: Response) {
     // var update=multer({dest:"./upload"}).single("photo");
        var studRepo= new StudentRepository();
        return studRepo.update(req,res).then(
            (studentData) => {
                let StudentData = studentData;
                console.log("update="+studentData.id);
                if (studentData.id) {
                    return "Date update Successfully!!!";
                  //  return (res.status(200).json({ response: { _isSucceed: true, _data: StudentData, _msg: "Date update Successfully!!!" } }));
                }
                else {
                    return "Something went wrong!!!";
                   // return res.status(400).json({ response: { _isSucceed: false, _data: [], _msg: "Something went wrong!!!" } });
                }
            }
        );
    } 
    
    async save(req: Request, res: Response) {
        var studRepo= new StudentRepository();
        console.log("cityid"+req.body.city)
        return studRepo.register(req,res).then(
            (studentData) => {
                let StudentData = studentData;
                console.log("sttttt="+studentData.id);
                if (studentData.id) {
                    return "Date Saved Successfully!!!";
                  //  return (res.status(200).json({ response: { _isSucceed: true, _data: StudentData, _msg: "Date Saved Successfully!!!" } }));
                }
                else {
                    return  "Something went wrong!!! ";
                    //return res.status(400).json({ response: { _isSucceed: false, _data: [], _msg: "Something went wrong!!!" } });
                }
            }
        );
    } 

    async remove(req: Request, res: Response) {
        var studRepo= new StudentRepository();
        return  studRepo.delete(req,res);
    } 

    async login(req:Request, res: Response) {  
        var studRepo= new StudentRepository();
        return studRepo.loginStudent(req).then(
            (studentData) => {
                let StudentData = studentData;
                console.log(StudentData);
              
                if (StudentData) {
                
                    const Cryptr = require('cryptr');
                    const cryptr = new Cryptr('ReallySecretKey');
                  
                    const decryptedString = cryptr.decrypt(StudentData.password);
                    console.log(decryptedString);

                    if (req.body.password=== decryptedString) {
                        return "Student login succesfully!!!";
                    } 
                    else {
                        return "invalid data";
                    }
        
                   //  return res.status(200).json({ response: { _isSucceed: true, _data: StudentData, _msg: "Student login succesfully!!!" } });
                }
                else {
                    return "invalid data";
                   // return res.status(400).json({ response: { _isSucceed: false, _data: [], _msg: "No Student data found" } });
                }
            }
        );
    }
    
 }