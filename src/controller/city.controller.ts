import { Request, Response } from "express";
import { CityRepository } from "../repository/city.repository";

export class CityController {
    
    async all(req: Request, res: Response) {
        var city= new CityRepository();
        return  city.findAllCity(req, res);
    } 
    
    async cityDetails(req: Request, res: Response) {
        var city= new CityRepository();
        return  city.findOneCity(req,res);
    } 
    
    async update(req: Request, res: Response) {
        var city= new CityRepository();
        return city.updateCity(req,res).then(
            (cityData) => {
                let CityData = cityData;
                console.log("update="+cityData.id);
                if (cityData.id) {
                    return "Date update Successfully!!!";
                  //  return (res.status(200).json({ response: { _isSucceed: true, _data: CityData, _msg: "Date update Successfully!!!" } }));
                }
                else {
                    return "Something went wrong!!!";
                   // return res.status(400).json({ response: { _isSucceed: false, _data: [], _msg: "Something went wrong!!!" } });
                }
            }
        );
    } 

    async save(req: Request, res: Response) {
        var city= new CityRepository();
        return city.addCity(req,res).then(
            (cityData) => {
                let CityData = cityData;
                console.log("addCity="+cityData.id);
                if (cityData.id) {
                    return "Date Saved Successfully!!!";
                   // return (res.status(200).json({ response: { _isSucceed: true, _data: CityData, _msg: "Date Saved Successfully!!!" } }));
                }
                else {
                    return "Something went wrong!!!";
                //    return res.status(400).json({ response: { _isSucceed: false, _data: [], _msg: "Something went wrong!!!" } });
                }
            }
        );
    } 

    async remove(req: Request, res: Response) {
        var city= new CityRepository();
        return  city.deleteCity(req,res);
    }     
 }