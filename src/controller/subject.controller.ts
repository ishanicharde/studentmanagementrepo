import { Request, Response } from "express";
import { SubjectRepository } from "../repository/subject.repository";

export class SubjectController {
    
    async all(req: Request, res: Response) {
        var subject= new SubjectRepository();
        return  subject.findAllSubjech(req, res);
    } 
    
    async subjectDetails(req: Request, res: Response) {
        var subject= new SubjectRepository();
        return  subject.findOneSubject(req,res);
    } 
    
    async update(req: Request, res: Response) {
        var subject= new SubjectRepository();
        return subject.updateSubject(req,res).then(
            (subjectData) => {
                let SubjectData = subjectData;
                console.log("update="+subjectData.id);
                if (subjectData.id) {
                    return "Date update Successfully!!!";
                  //  return (res.status(200).json({ response: { _isSucceed: true, _data: SubjectData, _msg: "Date update Successfully!!!" } }));
                }
                else {
                    return "Something went wrong!!!";
                   // return res.status(400).json({ response: { _isSucceed: false, _data: [], _msg: "Something went wrong!!!" } });
                }
            }
        );
    } 

    async save(req: Request, res: Response) {
        var subject= new SubjectRepository();
        return subject.addSubject(req,res).then(
            (subjectData) => {
                let SubjectData = subjectData;
                console.log("add="+subjectData.id);
                if (subjectData.id) {
                    return "Date Saved Successfully!!!";
                   // return (res.status(200).json({ response: { _isSucceed: true, _data: SubjectData, _msg: "Date Saved Successfully!!!" } }));
                }
                else {
                    return "Something went wrong!!!";
                //    return res.status(400).json({ response: { _isSucceed: false, _data: [], _msg: "Something went wrong!!!" } });
                }
            }
        );
    } 

    async remove(req: Request, res: Response) {
        var subject= new SubjectRepository();
        return  subject.deleteSubject(req,res);
    }     
 }