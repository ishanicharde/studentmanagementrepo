import { Request, Response } from "express";
import { DivisionRepository } from "../repository/division.repository";


export class DivisionController {
    
    async all(req: Request, res: Response) {
        var division= new DivisionRepository();
        return  division.findAllDivision(req,res);
    } 
    
    async divisionDetails(req: Request, res: Response) {
        var division= new DivisionRepository();
        return  division.findOneDivision(req,res);
    } 

    async save(req: Request, res: Response) {
        var division= new DivisionRepository();
        return division.addDivision(req,res).then(
            (divisionData) => {
                let DivisionData = divisionData;
                console.log("add="+divisionData.id);
                if (divisionData.id) {
                    return "Date Saved Successfully!!!";
                   // return (res.status(200).json({ response: { _isSucceed: true, _data: DivisionData, _msg: "Date Saved Successfully!!!" } }));
                }
                else {
                    return "Something went wrong!!!";
                //    return res.status(400).json({ response: { _isSucceed: false, _data: [], _msg: "Something went wrong!!!" } });
                }
            }
        );
    } 
    async update(req: Request, res: Response) {
        var division= new DivisionRepository();
        return division.updateDivision(req,res).then(
            (divisionData) => {
                let DivisionData = divisionData;
                console.log("update="+divisionData.id);
                if (divisionData.id) {
                    return "Date update Successfully!!!";
                  //  return (res.status(200).json({ response: { _isSucceed: true, _data: DivisionData, _msg: "Date update Successfully!!!" } }));
                }
                else {
                    return "Something went wrong!!!";
                   // return res.status(400).json({ response: { _isSucceed: false, _data: [], _msg: "Something went wrong!!!" } });
                }
            }
        );
    } 

    async remove(req: Request, res: Response) {
        var division= new DivisionRepository();
        return  division.deleteDivision(req,res);
    }     
 }