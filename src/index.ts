import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import { StudentRoutes } from "./route/student.route";
import {Student} from "./entity/StudentModel"
import { CityRoutes } from "./route/city.route";
import { DivisionRoutes } from "./route/division.route";

createConnection().then(async connection => {

    const studentRepository = connection.getRepository(Student);
    // create and setup express app
    const app = express();
    app.use(bodyParser.json());
    //app.use(express.json());
    app.use(express.json({limit: '50mb'}));
   app.use(express.urlencoded({limit: '50mb'}));
    app.use(express.static('files'));


   // register express routes from defined application routes 
   StudentRoutes.forEach(route => { 
    (app as any)[route.method](route.route, (req:   Request, res: Response, next: Function) => { 
      const result = (new (route.controller as any))[route.action](req, res, next); 
     console.log(result);
      if (result instanceof Promise) { 
         result.then(result => result !== null && result !== undefined ? res.send(result) : undefined); 
      } else if (result !== null && result !== undefined) { 
         res.json(result); 
      } 
   }); 
}); 


CityRoutes.forEach(route => { 
   (app as any)[route.method](route.route, (req:   Request, res: Response, next: Function) => { 
     const result = (new (route.controller as any))[route.action](req, res, next); 
    console.log(result);
     if (result instanceof Promise) { 
        result.then(result => result !== null && result !== undefined ? res.send(result) : undefined); 
     } else if (result !== null && result !== undefined) { 
        res.json(result); 
     } 
  }); 
}); 

DivisionRoutes.forEach(route => { 
   (app as any)[route.method](route.route, (req:   Request, res: Response, next: Function) => { 
     const result = (new (route.controller as any))[route.action](req, res, next); 
    console.log(result);
     if (result instanceof Promise) { 
        result.then(result => result !== null && result !== undefined ? res.send(result) : undefined); 
     } else if (result !== null && result !== undefined) { 
        res.json(result); 
     } 
  }); 
}); 


    const port = 3000;
    app.listen(port, () => {
        console.log(`Server is running on port ${port}.`);
      });
    
}).catch(error => console.log(error));
