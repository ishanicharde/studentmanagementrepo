import { DivisionController } from "../controller/division.controller";


export const DivisionRoutes = [{ 
    method: "get", 
    route: "/division", 
    controller: DivisionController, action: "all" 
 }, { 
    method: "get", 
    route: "/division/:id", 
    controller: DivisionController, action: "divisionDetails" 
 }, { 
    method: "post", 
    route: "/division/add", 
    controller: DivisionController, action: "save" 
 }, { 
    method: "post", 
    route: "/division/update/:id", 
    controller: DivisionController, action: "update" 
 }, { 
    method: "delete", 
    route: "/division/:id", 
    controller: DivisionController, action: "remove" 
 }];