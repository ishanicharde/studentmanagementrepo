import { CityController } from "../controller/city.controller";

export const CityRoutes = [{ 
    method: "get", 
    route: "/city", 
    controller: CityController, action: "all" 
 }, { 
    method: "get", 
    route: "/city/:id", 
    controller: CityController, action: "cityDetails" 
 }, { 
    method: "post", 
    route: "/city/add", 
    controller: CityController, action: "save" 
 },{ 
    method: "post", 
    route: "/city/update/:id", 
    controller: CityController, action: "update" 
 }, { 
    method: "delete", 
    route: "/city/:id", 
    controller: CityController, action: "remove" 
 }];