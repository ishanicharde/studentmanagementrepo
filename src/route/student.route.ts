import { StudentController } from "../controller/student.controller";

export const StudentRoutes = [{ 
      method: "get", 
      route: "/students", 
      controller: StudentController, action: "all" 
   }, { 
      method: "get", 
      route: "/students/:id", 
      controller: StudentController, action: "studentProfile" 
   }, { 
      method: "post", 
      route: "/students/register", 
      controller: StudentController, action: "save" 
   }, { 
      method: "post", 
      route: "/students/update/:id", 
      controller: StudentController, action: "update" 
   }, { 
      method: "delete", 
      route: "/students/:id", 
      controller: StudentController, action: "remove" 
   }, { 
      method: "post", 
      route: "/students/email", 
      controller: StudentController, action: "login" 
   }];
//       method: "post", 
//       route: "/file", 
//       controller: StudentController, action: "upload" 
