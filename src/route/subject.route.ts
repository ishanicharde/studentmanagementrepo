import { SubjectController } from "../controller/subject.controller";

export const SubjectRoutes = [{ 
    method: "get", 
    route: "/subject", 
    controller: SubjectController, action: "all" 
 }, { 
    method: "get", 
    route: "/subject/:id", 
    controller: SubjectController, action: "subjectDetails" 
 }, { 
    method: "post", 
    route: "/subject/add", 
    controller: SubjectController, action: "save" 
 }, { 
    method: "post", 
    route: "/subject/update/:id", 
    controller: SubjectController, action: "update" 
 }, { 
    method: "delete", 
    route: "/subject/:id", 
    controller: SubjectController, action: "remove" 
 }];