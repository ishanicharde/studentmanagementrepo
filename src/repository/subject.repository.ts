import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Subject } from "../entity/SubjectModel";

export class SubjectRepository {

    async findAllSubjech(request: Request, response: Response) {
        console.log("111")
        return await getRepository(Subject).find();
     }

    async findOneSubject(request: Request, response: Response) {
      console.log("222" + request.params.id)
      return await getRepository(Subject).findOne(request.params.id);
   }


    async updateSubject(request: Request, res: Response) {
        console.log("update="+request.body);
        let subjectToUpdate = await getRepository(Subject).findOne(request.params.id);
        subjectToUpdate.subjectname = request.body.subjectname;
        subjectToUpdate.division = request.body.division;
        return await getRepository(Subject).save(subjectToUpdate); 
    } 

    async addSubject(request: Request, res: Response) {
        console.log("Add="+request.body);
        return await getRepository(Subject).save(request.body); 
    } 

    async deleteSubject(request: Request, res: Response) {
      console.log("remove" + request.params.id) 
      let SubjectToRemove = await getRepository(Subject).findOne(request.params.id); 
      return await getRepository(Subject).remove(SubjectToRemove); 
   }
}