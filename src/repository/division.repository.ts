import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Division } from "../entity/DivisionModel";

export class DivisionRepository {

    async findAllDivision(request: Request,  response: Response) {
        console.log("111")
        return await getRepository(Division).find();
     }

    async findOneDivision(request: Request, response: Response) {
      console.log("222" + request.params.id)
      return await getRepository(Division).findOne(request.params.id);
   }


    async addDivision(request: Request, res: Response) {
        console.log("Add="+request.body);
        return await getRepository(Division).save(request.body); 
    } 
    
    async updateDivision(request: Request, res: Response) {
        console.log("update="+request.body);
        let divisionToUpdate = await getRepository(Division).findOne(request.params.id);
        divisionToUpdate.Divisionname = request.body.Divisionname;
        divisionToUpdate.roomnumber = request.body.roomnumber;
        return await getRepository(Division).save(divisionToUpdate); 
    } 

    async deleteDivision(request: Request, res: Response) {
      console.log("remove" + request.params.id) 
      let divisionToRemove = await getRepository(Division).findOne(request.params.id); 
      return await getRepository(Division).remove(divisionToRemove); 
   }
}