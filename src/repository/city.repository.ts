import { Request, Response} from "express";
import { getRepository } from "typeorm";
import { City } from "../entity/CityModel";

 export class CityRepository {

    async findAllCity(request: Request, response: Response) {
        console.log("111")
        return await getRepository(City).find();
     }

    async findOneCity(request: Request, response: Response) {
      console.log("222" + request.params.id)
      return await getRepository(City).findOne(request.params.id);
   }


    async updateCity(request: Request, res: Response) {
        console.log("update="+request.body);
        let cityToUpdate = await getRepository(City).findOne(request.params.id);
        cityToUpdate.cityname = request.body.cityname;
        cityToUpdate.country = request.body.country;
        return await getRepository(City).save(cityToUpdate); 
    } 

    async addCity(request: Request, res: Response) {
        console.log("AddCity="+request.body);
        return await getRepository(City).save(request.body); 
    } 

    async deleteCity(request: Request, res: Response) {
      console.log("remove" + request.params.id) 
      let cityToRemove = await getRepository(City).findOne(request.params.id); 
      return await getRepository(City).remove(cityToRemove); 
   }
}