import { Student } from "../entity/StudentModel";
import { Request, Response} from "express";
import { getRepository } from "typeorm";

 export class StudentRepository {

    async findAll(request: Request) {
        console.log("111")
        return await getRepository(Student).find();
     }

    async findOne(request: Request, response: Response) {
      console.log("222" + request.params.id)
      return await getRepository(Student).findOne(request.params.id);
   }


    async update(request: Request, res: Response) {
      console.log("update="+request.body);
      let studentToUpdate = await getRepository(Student).findOne(request.params.id); 
      studentToUpdate.phone = request.body.phone;
      studentToUpdate.address = request.body.address;
      studentToUpdate.photo = request.body.photo;
      return await getRepository(Student).save(studentToUpdate); 
  } 

    //   async update(request: Request, res: Response) {
    //     console.log("update="+request.body);
    //     let studentToUpdate = await getRepository(Student).findOne(request.params.id); 
    //     studentToUpdate.phone = request.body.phone;
    //     studentToUpdate.address = request.body.address;
    //     studentToUpdate.photo = request.body.photo;
    //     return await getRepository(Student).save(studentToUpdate); 
    // } 

    async register(request: Request, res: Response) {
        const Cryptr = require('cryptr');
        const cryptr = new Cryptr('ReallySecretKey');
        const encryptedString = cryptr.encrypt(request.body.password);
        request.body.password= encryptedString;
        console.log("333="+request.body.password);
        return await getRepository(Student).save(request.body); 
    } 

    async delete(request: Request, res: Response) {
      console.log("444" + request.params.id) 
      let studentToRemove = await getRepository(Student).findOne(request.params.id); 
      return await getRepository(Student).remove(studentToRemove); 
   }

    async loginStudent(request: any) {
        try {
            let stud = new Student();
            stud.email = request.body.email;
            console.log("555" + request.body.email) 
            return  await getRepository(Student).findOne({ where: { email: stud.email } });
        } catch (error) {
            return error;
        }
    }
}